import logging
import coloredlogs
import threading
import argparse
from time import sleep

from settings import Settings
import os

# -------------pre-------------------
# Logger
LOGFORMAT = "%(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
coloredlogs.install()
logger = logging.getLogger(__name__)
logger.info("Entering Pre Stage")
b_create_network = False

# Args
parser = argparse.ArgumentParser(description='Blockchain')
parser.add_argument("-n", "--noserver", help="Does not start the Server", action="store_true")
parser.add_argument("-d", "--debug", help="Set Logging on Debug-Level", action="store_true")
parser.add_argument("-ap", "--apiport", type=int, help="Port on which the API-Server should run")
parser.add_argument("-pp", "--p2pport", type=int, help="Port on which the P2P-Server should run")
parser.add_argument("-db", "--dbdir", type=str, help="Directory in which the Database should be saved")
parser.add_argument("-e", "--entrynode", type=str, help="Node-IP")
parser.add_argument("-c", "--createchain", action="store_true", help="Creates new blockchain")
parser.add_argument("-i", "--interactiveconsole", action="store_true", help="Interactive console")
parser.add_argument("-v", "--validate", action="store_true", help="validates blockchain")
parser.add_argument("-b", "--blockchain", action="store_true", help="Creates new blockchain network")
parser.add_argument("-k", "--genkeys", action="store_true", help="Only generates a keypair if no one is found")

args = parser.parse_args()
if args.noserver:
    logger.info("Server will not be started")
    Settings.NOSERVER = True
if args.debug:
    logger.info("Debug-Level is debug")
    Settings.LOG_LEVEL = "DEBUG"
    coloredlogs.set_level(Settings.LOG_LEVEL)
if args.apiport:
    logger.info("Port api %i", args.apiport)
    Settings.PORT_API = args.apiport
if args.p2pport:
    logger.info("Port P2P %i", args.p2pport)
    Settings.PORT_P2P = args.p2pport
if args.dbdir:
    logger.info("Directory %s", args.dbdir)
    Settings.change_dir(args.dbdir)
if args.entrynode:
    logger.info("Node ip: %s" % args.entrynode)
    Settings.ENTRY_NODE = args.entrynode
if args.createchain:
    logger.info("--createchain is set")
    b_create_network = True

# Test Dirs
for path in Settings.DIR_EXIST:
    logger.info(Settings.DB_DIR)
    if not os.path.exists(path):
        logger.info("Creating " + path)
        os.makedirs(path)

# ------------Imports---------------
logger.info("Importing")
# noinspection PyPep8
from api.api import start_api_server, add_admin_login
# noinspection PyPep8
from chain.crypto import Crypto
# noinspection PyPep8
from p2p.p2p import start_p2p, P2P

# -------------middle----------------
logger.info("Entering Config Stage")

# DEBUG
add_admin_login("admin", "admin")
# /DEBUG

Crypto.setup(b_create_network)
if args.genkeys:
    logger.info("Only generating keys!")
    exit()

p2p = P2P(b_new_blockchain=b_create_network)

# ------------start loops------------
logger.info("Entering loop Stage")
if args.validate:
    logger.info("Validate Chain")
    if p2p.blockchain.validate_chain():
        logger.info("Chain is valid")
    else:
        logger.info("Chain is not valid")
    Settings.NOSERVER = True  #

if Settings.NOSERVER:
    logger.info("Exit because NOSERVER flag was set")
    exit()

logger.info("Start API-Server")
t_api_server = threading.Thread(name="API-Server", target=start_api_server, args=(p2p.blockchain, p2p))
logger.info("Start P2P-Server")
t_p2p_server = threading.Thread(name="P2P-Server", target=start_p2p, args=(p2p,))

t_p2p_server.start()
t_api_server.start()

if not Settings.NOSERVER:
    sleep(2)
    threading.Thread(target=p2p.mine_block).start()

t_api_server.join()
t_p2p_server.join()
