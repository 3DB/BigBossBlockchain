"""
This class controls and represents the whole blockchain.

:author: Thogs
:date: 26.3.3.18
"""

import json
import logging

from chain.crypto import Crypto
from settings import Settings

from chain.models.block import Block
from chain.models.action import Action
from chain.models.signature import Signature
from chain.models.alias_signature import AliasSignature
from chain.models.action_addrole import ActionAddRole
from chain.models.action_changerolestate import ActionChangeRolestate
from chain.models.action_addchargingstation import ActionAddChargingStation
from chain.models.action_changechargingstationstate import ActionChangeChargingStationState
from chain.models.charging_station import ChargingStation
from chain.models.action_contract import ActionContract
from chain.models.contract import Contract
from chain.models.role import Role
from chain.models.hash_entry import HashEntry

logger = logging.getLogger("Blockchain")
db = Settings.BLOCKCHAIN_DB


class Blockchain:
    def __init__(self):

        self.action_id = 0
        self.tables = [Block, HashEntry, Role, Signature, Contract, AliasSignature]

        # Create actions for the genesis block
        r_id = 0
        actions = []
        for super_admin in Settings.SUPER_ADMINS:
            role = Role()
            role.pubkey = super_admin[0]
            role.address = super_admin[1]
            role.cs_server = "None"
            role.role_level = 0
            role.role_id = r_id
            role.role_state = "active"

            r_id += 1

            action = ActionAddRole()
            action.recipient_role = role
            action.set_hash()
            action.signature = Signature()
            action.signature.set(-1337, "DEADBEEF")

            actions.append(action)

        # Create genesis block
        self.genesis_block = Block()
        self.genesis_block.index = 0
        self.genesis_block.prev_hash = "ACC3553D"
        self.genesis_block.timestamp = 42
        self.genesis_block.set_signatures([])
        self.genesis_block.set_actions(actions)
        self.genesis_block.set_hash()
        self.genesis_block.set_final_hash()
        self.genesis_block.final_signature = Signature()
        self.genesis_block.final_signature.set(0, "FEEBDAED")

        db.create_tables(self.tables)

    def create_empty_blockchain(self):
        db.drop_tables(self.tables)
        db.create_tables(self.tables)
        self.genesis_block.save()
        self.execute_actions(self.genesis_block)

    # noinspection PyTypeChecker
    def get_block(self, block_index: int) -> Block:
        query = Block.select().where(Block.index == block_index)
        if len(query) > 0:
            return query[0]
        else:
            return None

    def validate_chain(self) -> bool:
        # TODO: Add try/catch for invalid input!!!

        # Clear
        db.drop_tables(self.tables)
        db.create_tables(self.tables)

        # Validate block by block
        block_index = 0
        block = self.get_block(block_index)
        prev_block = block

        while block is not None:
            if not self.validate_next_block(prev_block, block):
                return False

            # Execute actions
            self.execute_actions(block)

            block_index += 1
            prev_block = block
            block = self.get_block(block_index)

        return True

    def validate_genesis_block(self, block: Block) -> bool:
        # Correct hash?
        if not block.verify_hash():
            return False

        # Correct final?
        if not block.verify_final_hash():
            return False

        # Equals stored genesis?
        if block.final_hash != self.genesis_block.final_hash:
            return False

        # TODO: Add extended validation

        return True

    def validate_next_block(self, prev_block: Block, block: Block, b_final_validation=True) -> bool:

        # TODO: Add extended validation

        # Correct index?
        if block.index != prev_block.index + 1:
            return False

        # Correct prevHash?
        if block.prev_hash != prev_block.final_hash:
            return False

        # Correct hash?
        if not block.verify_hash():
            return False

        # TODO: Add timestamp validation

        # Validate actions
        actions = block.get_actions()
        action_ids = {}
        for action in actions:
            action = self.action_from_json(action)

            # Valid?
            if not self.validate_action(action):
                return False

            # Collect action_ids
            if action.signature.role_id not in action_ids:
                action_ids[action.signature.role_id] = [action.action_id]
            else:
                action_ids[action.signature.role_id].append(action.action_id)

        # Validate action_ids
        for role_id in action_ids.keys():
            action_ids[role_id].sort()
            cur_action_id = self.get_role(role_id).cur_action_id

            for action_id in action_ids[role_id]:
                if action_id != cur_action_id:
                    return False

                cur_action_id += 1

        # Signatures
        signatures_raw = block.get_signatures()
        signatures = []

        # Remove duplicates
        for signature_raw in signatures_raw:
            b_unique = True
            for signature in signatures:
                if signature_raw.role_id == signature.role_id:
                    b_unique = False
                    break

            if b_unique:
                signatures.append(signature_raw)

        # Validate final elements
        if b_final_validation:

            # Enough signatures?
            if float(len(signatures)) < self.get_roles_count() * Settings.MINIMUM_SIGNATURES_PER_BLOCK:
                return False

            # Valid final hash?
            if not block.verify_final_hash():
                return False

            # Valid final signature?
            if not Crypto.verify(block.final_hash, block.final_signature.signature,
                                 self.get_role(block.final_signature.role_id).pubkey):
                return False

        # Valid signatures?
        for signature in signatures:

            # Signer exists?
            if self.get_role(signature.role_id) is None:
                return False

            # Signer is active?
            if not self.get_role(signature.role_id).role_state == "active":
                return False

            # Valid signature?
            if not Crypto.verify(block.hash, signature.signature, self.get_role(signature.role_id).pubkey):
                return False

        # Everything's valid!
        return True

    def validate_action(self, action: Action) -> bool:

        # Correct hash?
        if not action.verify_hash():
            return False

        # Existing ID?
        if self.hash_entry_exists(action.action_type, action.hash):
            return False

        # Rolechanges
        if isinstance(action, ActionAddRole):

            # Active role?
            if self.get_role(action.signature.role_id).role_state != "active":
                return False

            # Correctly signed?
            if not Crypto.verify(action.hash, action.signature.signature, self.get_role(action.signature.role_id).pubkey):
                return False

            # Allowed to?
            if self.get_role(action.signature.role_id).role_level != 0:
                return False

            # Wrong type?
            if action.action_type != 1:
                return False

        # Contract
        elif isinstance(action, ActionContract):

            # Is allowed to?
            if self.get_role(action.contract.final_signature.role_id).role_level > 1:   # TODO: Change debug!
                return False

            # Active role?
            if self.get_role(action.contract.final_signature.role_id).role_state != "active":
                return False

            # Correctly signed action?
            if not Crypto.verify(action.hash, action.signature.signature,
                                 self.get_role(action.signature.role_id).pubkey):
                return False

            print("Doomed: " + action.contract.user_signature.authority_signature.signature)

            # Correctly signed alias?
            if not Crypto.verify(action.contract.user_signature.hash,
                                 action.contract.user_signature.authority_signature.signature,
                                 self.get_role(action.contract.user_signature.authority_signature.role_id).pubkey):
                return False

            # Correctly signed contract?
            if not Crypto.verify(action.contract.user_hash, action.contract.user_signature.contract_signature,
                                 action.contract.user_signature.alias):
                return False

            # Wrong type?
            if action.action_type != 2:
                return False

        # ChangeRolestate
        elif isinstance(action, ActionChangeRolestate):

            # Is allowed to?
            if self.get_role(action.signature.role_id).role_level != 0:
                return False

            # Active role?
            if self.get_role(action.signature.role_id).role_state != "active":
                return False

            # Correctly signed?
            if not Crypto.verify(action.hash, action.signature.signature, self.get_role(action.signature.role_id).pubkey):
                return False

            # Role exists?
            if self.get_role(action.role_id) == None:
                return False

            # Wrong type?
            if action.action_type != 3:
                return False

        # AddChargingStation
        elif isinstance(action, ActionAddChargingStation):

            # Is allowed to?
            if self.get_role(action.signature.role_id).role_level > 1:  # TODO: Change debug!
                return False

            # Active role?
            if self.get_role(action.signature.role_id).role_state != "active":
                return False

            # Correctly signed?
            if not Crypto.verify(action.hash, action.signature.signature,
                                 self.get_role(action.signature.role_id).pubkey):
                return False

            # Station belongs to signer?
            if action.signature.role_id != action.charging_station.authority_id:
                return False

            # Station doesn't exist?
            if self.get_charging_station(action.charging_station.authority_id, action.charging_station.station_id) != None:
                return False

            # Wrong type?
            if action.action_type != 4:
                return False

        # ChangeChargingStationState
        elif isinstance(action, ActionChangeChargingStationState):

            # Is allowed to?
            if self.get_role(action.signature.role_id).role_level > 1: # TODO: Change debug!
                return False

            # Active role?
            if self.get_role(action.signature.role_id).role_state != "active":
                return False

            # Correctly signed?
            if not Crypto.verify(action.hash, action.signature.signature,
                                 self.get_role(action.signature.role_id).pubkey):
                return False

            # Station belongs to signer?
            if action.signature.role_id != action.authority_id:
                return False

            # Station exists?
            if self.get_charging_station(action.authority_id, action.station_id) == None:
                return False

        # Unknown type
        else:
            return False

        # Valid
        return True

    def validate_block(self, block: Block, b_final_validation=True) -> bool:
        if block.index == 0:
            return self.validate_genesis_block(block)

        else:
            return self.validate_next_block(self.get_last_block(), block, b_final_validation)

    def action_from_json(self, json_data: str):
        data = json.loads(json_data)
        action_type = data["action_type"]

        action = None

        if action_type == 1:
            action = ActionAddRole()
            action.from_json(data)

        elif action_type == 2:
            action = ActionContract()
            action.from_json(data)

        elif action_type == 3:
            action = ActionChangeRolestate()
            action.from_json(data)

        elif action_type == 4:
            action = ActionAddChargingStation()
            action.from_json(data)

        elif action_type == 5:
            action = ActionChangeChargingStationState()
            action.from_json(data)

        return action

    def execute_actions(self, block: Block):
        actions = block.get_actions()
        for action in actions:
            action = self.action_from_json(action)

            if isinstance(action, ActionAddRole):
                action.recipient_role.save()
                if action.recipient_role.pubkey == Crypto.get_public_key():
                    Crypto.set_authority_id(action.recipient_role.role_id)

            elif isinstance(action, ActionContract):

                # Is our user?
                if action.contract.user_signature.authority_signature.role_id == Crypto.get_authority_id():
                    action.contract.save()

            elif isinstance(action, ActionChangeRolestate):
                role = self.get_role(action.role_id)
                role.role_state = action.role_state
                role.save()

            elif isinstance(action, ActionAddChargingStation):
                action.charging_station.save()

            elif isinstance(action, ActionChangeChargingStationState):
                cs = self.get_charging_station(action.authority_id, action.station_id)
                cs.activity_state = action.activity_state
                cs.save()

            # Save hash
            entry = HashEntry()
            entry.hash = action.hash
            entry.type = action.action_type
            entry.save()

            # Update action_id
            role = self.get_role(action.signature.role_id)
            if role is not None:
                if role.cur_action_id <= action.action_id:
                    role.cur_action_id = action.action_id + 1
                    role.save()

    # noinspection PyTypeChecker
    def get_role(self, role_id: int) -> Role:
        query = Role.select().where(Role.role_id == role_id)
        if len(query) > 0:
            return query[0]
        else:
            return None

    # noinspection PyTypeChecker
    def get_charging_station(self, authority_id: int, station_id: int) -> ChargingStation:
        query = ChargingStation.select().where(ChargingStation.authority_id == authority_id
                                               and ChargingStation.station_id == station_id)
        if len(query) > 0:
            return query[0]
        else:
            return None

    def get_all_roles(self) -> list:
        return Role.select().where(True)

    def get_active_roles(self) -> list:
        return Role.select().where(Role.role_state == "active")

    def get_roles_count(self) -> int:
        return len(self.get_active_roles())

    def hash_entry_exists(self, entry_type: int, entry_hash: str) -> bool:
        query = HashEntry.select().where(HashEntry.type == entry_type and HashEntry.hash == entry_hash)
        if len(query) > 0:
            return True
        else:
            return False

    def gen_add_role(self, pubkey: str, role_level: int, p2p_address: str, cs_server: str, role_state="active"):
        role = Role()
        role.pubkey = pubkey
        role.role_level = role_level
        role.address = p2p_address
        role.cs_server = cs_server
        role.role_state = role_state

        # Set next role id
        roles = Role.select().where(True)

        max_id = 0
        for q_role in roles:
            if q_role.role_id > max_id:
                max_id = q_role.role_id

        role.role_id = max_id + 1

        # Generate action
        action = ActionAddRole()
        action.action_id = self.action_id
        self.action_id += 1
        action.recipient_role = role
        action.donor_id = Crypto.get_authority_id()
        action.set_hash()
        action.sign()

        return action

    def gen_change_rolestate(self, role_id: int, role_state: str):
        action = ActionChangeRolestate()
        action.action_id = self.action_id
        self.action_id += 1
        action.role_id = role_id
        action.role_state = role_state
        action.set_hash()
        action.sign()

        return action

    def gen_add_charging_station(self, station_id: int, info_address: str, gps_latitude: float, gps_longitude: float, activity_state: str):
        action = ActionAddChargingStation()
        action.action_id = self.action_id
        self.action_id += 1
        action.charging_station = ChargingStation()
        action.charging_station.authority_id = Crypto.get_authority_id()
        action.charging_station.station_id = station_id
        action.charging_station.info_address = info_address
        action.charging_station.gps_latitude = gps_latitude
        action.charging_station.gps_longitude = gps_longitude
        action.charging_station.activity_state = activity_state
        action.set_hash()
        action.sign()

        return action

    def gen_change_charging_station_state(self, station_id: int, activity_state: str):
        action = ActionChangeChargingStationState()
        action.action_id = self.action_id
        self.action_id += 1
        action.authority_id = Crypto.get_authority_id()
        action.station_id = station_id
        action.activity_state = activity_state
        action.set_hash()
        action.sign()

        return action

    def gen_add_contract(self, contract: Contract):
        action = ActionContract()
        action.action_id = self.action_id
        self.action_id += 1
        action.contract = contract
        action.set_hash()
        action.sign()

        return action

    def get_last_block(self) -> Block:
        return self.get_block(self.blockchain_count() - 1)

    def add_block(self, block: Block):
        block.save()

        self.execute_actions(block)

    def update_action_id(self, block: Block):
        max_action_id = self.own_action_id() - 1
        for action in block.get_actions():
            action = self.action_from_json(action)

            if action.signature.role_id == Crypto.get_authority_id():
                if action.action_id > max_action_id:
                    max_action_id = action.action_id

        own_role = self.get_role(Crypto.get_authority_id())
        own_role.cur_action_id = max_action_id + 1
        own_role.save()

    def own_action_id(self) -> int:
        return self.get_role(Crypto.get_authority_id()).cur_action_id

    def create_block(self, actions: list) -> Block:
        block = Block()
        block.index = self.blockchain_count()
        block.prev_hash = self.get_block(self.blockchain_count() - 1).final_hash
        block.set_timestamp()
        block.set_actions(actions)
        block.set_signatures([])
        block.set_hash()
        block.sign_trusted_block()

        return block

    def blockchain_exists(self):
        query = Block.select().where(True)
        return len(query) > 0

    def blockchain_count(self):
        return Block.select().count()

    def is_alias_unique(self, alias: str):
        return len(AliasSignature.select().where(AliasSignature.alias == alias)) == 0


db.create_tables([Block])
