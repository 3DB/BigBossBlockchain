"""
A representation of a block.

:author: Thogs
:date: 25.3.18
"""

import json
import logging
from time import time
from peewee import *

from chain.models.signature import Signature
from chain.crypto import Crypto
from settings import Settings

logger = logging.getLogger("Block")
db = Settings.BLOCKCHAIN_DB


class Block(Model):
    index = IntegerField()
    prev_hash = CharField()
    timestamp = IntegerField()
    _actions = CharField()
    hash = CharField()
    _signatures = CharField()
    final_hash = CharField()
    final_signature = ForeignKeyField(Signature)

    def from_json(self, json_block: str):
        try:
            data = json.loads(json_block)
            self.index = data["index"]
            self.hash = data["hash"]
            self.prev_hash = data["prev_hash"]
            self.timestamp = data["timestamp"]
            self._actions = data["actions"]
            self._signatures = data["signatures"]
            self.final_hash = data["final_hash"]

            if data["final_signature"] is not None:
                self.final_signature = Signature()
                self.final_signature.from_json(data["final_signature"])

        except:
            error_msg = "Invalid JSON data - Couldn't create block"
            logger.error(error_msg)
            raise ValueError(error_msg)

    def to_json(self) -> str:
        json_raw = {}
        json_raw["index"] = self.index
        json_raw["hash"] = self.hash
        json_raw["prev_hash"] = self.prev_hash
        json_raw["timestamp"] = self.timestamp
        json_raw["actions"] = self._actions
        json_raw["signatures"] = self._signatures
        json_raw["final_hash"] = self.final_hash

        try:
            json_raw["final_signature"] = self.final_signature.to_json()
        except:
            json_raw["final_signature"] = None

        return json.dumps(json_raw, sort_keys=True)

    def get_actions(self) -> list:
        return json.loads(self._actions)

    def set_actions(self, action_list):
        action_str = []
        for action in action_list:
            action_str.append(action.to_json())

        self._actions = json.dumps(action_str, sort_keys=True)

    def get_signatures(self) -> list:
        signature_list = []
        for signature_str in json.loads(self._signatures):
            signature = Signature()
            signature.from_json(signature_str)
            signature_list.append(signature)

        return signature_list

    def set_signatures(self, signature_list):
        signature_str = []
        for signature in signature_list:
            signature_str.append(signature.to_json())

        self._signatures = json.dumps(signature_str, sort_keys=True)

    def set_timestamp(self):
        self.timestamp = int(time())

    def sign_trusted_block(self):
        signature_list = self.get_signatures()
        signature = Signature()
        signature.set(Crypto.get_authority_id(), Crypto.sign(self.hash))
        signature_list.append(signature)
        self.set_signatures(signature_list)

    def _create_final_hash(self):
        block = {}

        block["hash"] = self.hash
        block["signatures"] = self._signatures

        return Crypto.sha256_json(block)

    def set_final_hash(self):
        self.final_hash = self._create_final_hash()

    def verify_final_hash(self):
        return self.final_hash == self._create_final_hash()

    def sign_final(self):
        self.final_signature = Signature()
        self.final_signature.set(Crypto.get_authority_id(), Crypto.sign(self.final_hash))

    def _create_hash(self) -> str:
        block = {}

        block["index"] = self.index
        block["prevHash"] = self.prev_hash
        block["timestamp"] = self.timestamp
        block["actions"] = self._actions

        return Crypto.sha256_json(block)

    def set_hash(self):
        self.hash = self._create_hash()

    def verify_hash(self) -> bool:
        return self._create_hash() == self.hash

    def save(self, force_insert=False, only=None):
        self.final_signature.save()
        Model.save(self, force_insert=force_insert, only=only)

    class Meta:
        database = db
        primary_key = False
