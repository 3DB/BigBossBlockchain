"""
Used for saving the hashes in a table.

:author: Thogs
:date: 27.3.18
"""

from peewee import *

from settings import Settings

db = Settings.BLOCKCHAIN_DB


class HashEntry(Model):
    hash = CharField()
    type = IntegerField()

    class Meta:
        database = db
