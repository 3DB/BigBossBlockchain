"""
An action of adding a charging station to the network.

:author: Thogs
:date: 25.3.18
"""

import json
import logging
from peewee import *
from settings import Settings

from chain.models.charging_station import ChargingStation
from chain.models.action import Action
from chain.crypto import Crypto

logger = logging.getLogger("ActionAddChargingStation")
db = Settings.BLOCKCHAIN_DB


class ActionAddChargingStation(Action, Model):

    action_type = IntegerField(default=4)
    charging_station = ForeignKeyField(ChargingStation)

    def _create_hash(self):
        data = self._create_hash_super()

        data["recipient_role"] = self.charging_station.to_json()

        return Crypto.sha256_json(data)

    def from_json(self, data: dict):  # -> ActionAddChargingStation
        try:
            self._from_json_base(data)

            self.charging_station = ChargingStation()
            self.charging_station.from_json(data["charging_station"])

        except:
            error_msg = "Invalid JSON data - Couldn't create ActionAddChargingStation"
            logger.error(error_msg)
            raise ValueError(error_msg)

    def to_json(self) -> str:
        data = self._to_json_base()

        data["charging_station"] = self.charging_station.to_json()

        return json.dumps(data, sort_keys=True)

    def save(self, force_insert=False, only=None):
        self.charging_station.save()
        Model.save(self, force_insert=force_insert, only=only)