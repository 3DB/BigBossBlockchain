"""
An action of changing someone's privileges on the blockchain.

:author: Thogs
:date: 25.3.18
"""

import json
import logging
from peewee import *
from settings import Settings

from chain.models.signature import Signature
from chain.models.action import Action
from chain.models.role import Role
from chain.crypto import Crypto

logger = logging.getLogger("ActionRolechange")
db = Settings.BLOCKCHAIN_DB


class ActionAddRole(Action, Model):

    action_type = IntegerField(default=1)
    recipient_role = ForeignKeyField(Role)

    def _create_hash(self):
        data = self._create_hash_super()

        data["recipient_role"] = self.recipient_role.to_json()

        return Crypto.sha256_json(data)

    def from_json(self, data: dict):  # -> ActionAddRole
        try:
            self._from_json_base(data)

            self.recipient_role = Role()
            self.recipient_role.from_json(data["recipient_role"])

        except:
            error_msg = "Invalid JSON data - Couldn't create ActionAddRole"
            logger.error(error_msg)
            raise ValueError(error_msg)

    def to_json(self) -> str:
        data = self._to_json_base()

        data["recipient_role"] = self.recipient_role.to_json()

        return json.dumps(data, sort_keys=True)
