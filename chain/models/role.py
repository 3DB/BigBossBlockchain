"""
Represents a role; someone with special privileges on the blockchain.

:author: Thogs
:date: 26.3.18
"""

import json
import logging
from peewee import *

from settings import Settings

logger = logging.getLogger("Role")
db = Settings.BLOCKCHAIN_DB


class Role(Model):
    pubkey = CharField()
    role_level = IntegerField()
    role_id = IntegerField()
    address = CharField()
    cs_server = CharField()
    role_state = CharField()
    cur_action_id = IntegerField(default=0)

    def from_json(self, json_data: str):
        try:
            data = json.loads(json_data)

            self.pubkey = data["pubkey"]
            self.role_level = data["role"]
            self.role_id = data["id"]
            self.address = data["address"]
            self.cs_server = data["cs_server"]
            self.role_state = data["role_state"]
            self.cur_action_id = data["cur_action_id"]

        except:
            error_msg = "Invalid JSON data - Couldn't create Role"
            logger.error(error_msg)
            raise ValueError(error_msg)

    def to_json(self) -> str:
        json_raw = {}

        json_raw["pubkey"] = self.pubkey
        json_raw["role"] = self.role_level
        json_raw["id"] = self.role_id
        json_raw["address"] = self.address
        json_raw["cs_server"] = self.cs_server
        json_raw["role_state"] = self.role_state
        json_raw["cur_action_id"] = self.cur_action_id

        return json.dumps(json_raw, sort_keys=True)

    class Meta:
        database = db


db.create_tables([Role])
