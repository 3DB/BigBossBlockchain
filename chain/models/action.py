"""
Abstract representation of any action on the blockchain.

:author: Thogs
:date: 25.3.18
"""

import logging
from peewee import *

from chain.crypto import Crypto
from chain.models.signature import Signature
from settings import Settings

logger = logging.getLogger("Action")
db = Settings.BLOCKCHAIN_DB

class Action(Model):
    action_type = IntegerField()
    action_id = IntegerField()
    hash = CharField()
    signature = ForeignKeyField(Signature)

    # Abstract
    def sign(self):
        self.signature = Signature()
        self.signature.set(Crypto.get_authority_id(), Crypto.sign(self.hash))

    # Abstract
    def _create_hash(self) -> str:
        pass

    # Abstract
    def to_json(self) -> str:
        pass

    def set_hash(self):
        self.hash = self._create_hash()

    def verify_hash(self) -> bool:
        return self._create_hash() == self.hash

    def _create_hash_super(self) -> dict:
        data = {}

        data["action_type"] = self.action_type
        data["action_id"] = self.action_id

        return data

    def _from_json_base(self, data: dict):
        self.action_type = data["action_type"]
        self.hash = data["hash"]
        self.signature = Signature()
        self.signature.from_json(data["signature"])
        self.action_id = data["action_id"]

    def _to_json_base(self) -> dict:
        data = {}

        data["action_type"] = self.action_type
        data["hash"] = self.hash
        data["signature"] = self.signature.to_json()
        data["action_id"] = self.action_id

        return data

    def save(self, force_insert=False, only=None):
        self.signature.save()
        Model.save(self, force_insert=force_insert, only=only)

    class Meta:
        database = db