"""
An action of changing the activity state of a charging station.

:author: Thogs
:date: 25.3.18
"""

import json
import logging
from peewee import *
from settings import Settings

from chain.models.action import Action
from chain.crypto import Crypto

logger = logging.getLogger("ActionChangeRChargingStationState")
db = Settings.BLOCKCHAIN_DB


class ActionChangeChargingStationState(Action, Model):

    action_type = IntegerField(default=5)
    authority_id = IntegerField()
    station_id = IntegerField()
    activity_state = CharField()

    def _create_hash(self):
        data = self._create_hash_super()

        data["authority_id"] = self.authority_id
        data["station_id"] = self.station_id
        data["activity_state"] = self.activity_state

        return Crypto.sha256_json(data)

    def from_json(self, data: dict):  # -> ActionChangeChargingStationState
        try:
            self._from_json_base(data)

            self.authority_id = data["authority_id"]
            self.station_id = data["station_id"]
            self.activity_state = data["activity_state"]

        except:
            error_msg = "Invalid JSON data - Couldn't create ActionChangeChargingStationState"
            logger.error(error_msg)
            raise ValueError(error_msg)

    def to_json(self) -> str:
        data = self._to_json_base()

        data["authority_id"] = self.authority_id
        data["station_id"] = self.station_id
        data["activity_state"] = self.activity_state

        return json.dumps(data, sort_keys=True)
