"""
Does all the crypto stuff we need, e.g. signing, verifying.

:author: Thogs
:date: 25.3.18
"""

import hashlib
import json
import base64
from peewee import *

from settings import Settings

db = SqliteDatabase(Settings.KEY_FILE)


from ecdsa import SigningKey, VerifyingKey, NIST521p
import ecdsa

class Crypto:
    _key_pair = None
    _signing_key = None

    @staticmethod
    def setup(b_create_network: bool):
        if not Crypto._keypair_exists():
            Crypto._key_pair = KeyPair()
            Crypto._signing_key = SigningKey.generate(curve=NIST521p, hashfunc=hashlib.sha256)
            Crypto._key_pair.signing_key = Crypto.key_to_base64_der(Crypto._signing_key)

            if b_create_network:
                Crypto._key_pair.authority_id = 0

            Crypto._key_pair.save()

            with open(Settings.PUBKEY_TXT, "w") as f:
                f.write(Crypto.key_to_base64_der(Crypto._signing_key.get_verifying_key()))

        else:
            Crypto._key_pair = KeyPair.select()[0]
            Crypto._signing_key = Crypto.signing_key_from_base64_der(Crypto._key_pair.signing_key)

    @staticmethod
    def sign(text: str) -> str:
        return base64.standard_b64encode(Crypto._signing_key.sign(text.encode("utf-8"), hashfunc=hashlib.sha256, sigencode=ecdsa.util.sigencode_der)).decode("utf-8")

    @staticmethod
    def verify(plain_text: str, signed_text: str, pubkey: str):
        signature = base64.standard_b64decode(signed_text)
        try:
            return Crypto.verifying_key_from_base64_der(pubkey).verify(signature, plain_text.encode("utf-8"), hashfunc=hashlib.sha256, sigdecode=ecdsa.util.sigdecode_der)
        except:
            return False

    @staticmethod
    def key_to_base64_der(key) -> str:
        return base64.standard_b64encode(key.to_der()).decode("utf-8")

    @staticmethod
    def verifying_key_from_base64_der(base64_str: str) -> VerifyingKey:
        return VerifyingKey.from_der(base64.standard_b64decode(base64_str.encode("utf-8")))

    @staticmethod
    def signing_key_from_base64_der(base64_str: str) -> SigningKey:
        return SigningKey.from_der(base64.standard_b64decode(base64_str.encode("utf-8")))

    @staticmethod
    def get_authority_id() -> int:
        return Crypto._key_pair.authority_id

    @staticmethod
    def set_authority_id(authority_id: int):
        Crypto._key_pair.authority_id = authority_id
        Crypto._key_pair.save()

    @staticmethod
    def get_public_key():
        return Crypto.key_to_base64_der(Crypto._signing_key.get_verifying_key())

    @staticmethod
    def sha256_text(text: str) -> str:
        return hashlib.sha256(text.encode("utf-8")).hexdigest()

    @staticmethod
    def sha256_json(json_data: dict) -> str:
        return Crypto.sha256_text(json.dumps(json_data, sort_keys=True))

    @staticmethod
    def _keypair_exists() -> bool:
        query = KeyPair.select().where(True)
        return len(query) > 0

class KeyPair(Model):
    signing_key = CharField()
    authority_id = IntegerField(default=-1)

    class Meta:
        database = db


db.create_tables([KeyPair])
