from flask import abort, request
from flask.app import Flask
from threading import Thread
from time import time, sleep
import requests
import random
import logging
import json
import os

from chain.models.block import Block
from chain.models.action import Action
from chain.models.signature import Signature
from chain.blockchain import Blockchain
from chain.crypto import Crypto
from settings import Settings

logger = logging.getLogger(__name__)
p2p_server = Flask('p2p-Server')
p2p = None

def start_p2p(p_p2p):
    global p2p
    global p2p_server
    p2p = p_p2p
    p2p_server.run(port=Settings.PORT_P2P, host='0.0.0.0')
    logging.debug("PID:", os.getpid())
    logging.debug("Werkzeug subprocess:", os.environ.get("WERKZEUG_RUN_MAIN"))
    logging.debug("Inherited FD:", os.environ.get("WERKZEUG_SERVER_FD"))
    logging.info("API-Server started")


# Flask Methods
@p2p_server.route("/get_block", methods=['POST', 'GET'])
def a_get_block():
    values = request.get_json()
    if "block_index" not in values:
        logger.debug("No Block_index in values")
        abort(400)  # TODO html

    else:
        block_index = values["block_index"]

        try:
            logger.debug("Is Block_index an int?")
            block_index = int(block_index)
        except:
            logger.debug("Bad format block_index")
            abort(400)
        block = p2p.blockchain.get_block(block_index)

        if block is None:
            logger.debug("Block_index: %i does not exist" % block_index)
            return "", 204

        logger.info("Sending Block %i" % block_index)
        return block.to_json()


@p2p_server.route("/post_action", methods=['POST', 'GET'])  # TODO: Change debug
def a_post_action():
    #try:
        logger.debug("Try to load Action from json")
        action = p2p.blockchain.action_from_json(json.dumps(request.get_json()))

        if p2p.blockchain.validate_action(action):
            logger.info("Adding Action to outstanding_actions")
            p2p.outstanding_actions.append(action)
            return Settings.success_json()

        else:
            logger.debug("Not valid action")
            abort(400)

    #except Exception as e:
    #    logger.debug("Loading failed %s", e)
    #    abort(400)


@p2p_server.route("/get_nodes", methods=['POST', 'GET'])
def a_get_nodes():
    logger.debug("Sending nodes")
    return json.dumps(p2p.nodes)


@p2p_server.route("/sign_block", methods=['POST', 'GET'])
def a_sign_block():
    logger.debug("Try to sign a Block")
    block = Block()

    try:
        logger.debug("Try to convert json to block")
        block.from_json(json.dumps(request.get_json()))
        logger.debug("Try to validate block")

        miner_id = p2p.get_miner_id()
        b_found_miner = False
        for signature in block.get_signatures():
            if p2p.get_sorted_authority_id(signature.role_id) == miner_id:
                b_found_miner = True

        if b_found_miner and p2p.blockchain.validate_block(block, False):
            logger.debug("Validate Block, signing block")
            logger.info("Signing Block %i" % block.index)
            block.sign_trusted_block()

            for signature in block.get_signatures():
                if signature.role_id == Crypto.get_authority_id():
                    return signature.to_json()

        else:
            logger.debug("Bad Block %i", block.index)
            abort(400)

    except ValueError:
        logger.debug("Bad json block in sign_block")
        abort(400)


@p2p_server.route("/post_block", methods=['POST', 'GET'])
def a_post_block():
    logger.debug("Try to add Block")
    block = Block()
    try:
        logger.debug("Try to convert Block from request")
        block.from_json(json.dumps(request.get_json()))

        if p2p.blockchain.blockchain_count() < block.index:
            p2p.get_blockchain(p2p.blockchain.blockchain_count() - 1)

        if p2p.blockchain.validate_block(block):
            logger.debug("Try to add the block %i to the block chain" % block.index)
            p2p.blockchain.add_block(block)

            # Remove corresponding outstanding actions
            updated_out_actions = []
            for out_action in p2p.outstanding_actions:
                b_unique = True
                for action in block.get_actions():
                    action = p2p.blockchain.action_from_json(action)

                    if action.hash == out_action.hash:
                        b_unique = False

                if b_unique:
                    updated_out_actions.append(out_action)

            p2p.outstanding_actions = updated_out_actions

            p2p.load_nodes()

            return Settings.success_json()

        else:
            abort(400)

    except Exception as e:
        logger.info("Exception in a_post_block %s" % e)
        abort(400)


@p2p_server.route("/ping")
def a_ping():
    return Settings.success_json()


# Class
class P2P:
    def __init__(self, b_new_blockchain=False):
        logger.debug("Init P2P Class")
        self.nodes = {}
        self.sorted_nodes = []
        self.blockchain = Blockchain()

        if b_new_blockchain:
            self.blockchain.create_empty_blockchain()
            self.load_nodes()
        else:
            self.fill_blockchain()

        self.outstanding_actions = []

    def fill_blockchain(self):
        if Crypto.get_authority_id() != -1:
            self.blockchain.action_id = self.blockchain.own_action_id()

        if not self.blockchain.blockchain_exists():
            if not Settings.ENTRY_NODE is None:
                self.nodes = {0: Settings.ENTRY_NODE}
            else:
                self.nodes = {0: Settings.SUPER_ADMINS[0][1]}

            self.get_blockchain()

        else:
            self.load_nodes()
            self.get_blockchain(self.blockchain.blockchain_count())

    def _get_own_address(self) -> str:
        logger.debug("Executing _get_own_address")
        return self.blockchain.get_role(Crypto.get_authority_id()).address

    def _node_to_http(self, node: str):
        logger.debug("Executing _node_to_http with node %s" % node)
        return "http://" + node

    def mine_block(self):
        logger.debug("Executing mine_block")
        while True:
            if self.get_miner_id() == self.get_sorted_authority_id(Crypto.get_authority_id()):
                block = self.blockchain.create_block(self.outstanding_actions)

                self.post_sign(block)

                if self.blockchain.validate_block(block):
                    self.post_block(block)

            sleep((Settings.TIME_PER_BLOCK - int(time()) % Settings.TIME_PER_BLOCK) + 1)

    def get_miner_id(self):
        t = int(time())
        t = int(t / Settings.TIME_PER_BLOCK)

        miner_id = t % len(self.sorted_nodes)

        return miner_id

    def get_sorted_authority_id(self, normal_id):
        role = self.blockchain.get_role(normal_id)
        if role is not None:
            return self.sorted_nodes.index(role.address)
        else:
            return None

    def load_nodes(self):
        roles = self.blockchain.get_all_roles()

        for role in roles:
            if role.role_state == "active":
                self.nodes.update({role.role_id : role.address})

        self._update_sorted_nodes()

    def _get_next_miners(self, count: int) -> list:
        if count > len(self.sorted_nodes):
            count = len(self.sorted_nodes)

        next_miners = []
        miner_id = self.get_miner_id()
        for i in range(count):
            next_miners.append(self.sorted_nodes[(miner_id + i) % len(self.sorted_nodes)])

        return next_miners

    def _update_sorted_nodes(self):
        self.sorted_nodes = []

        for key in sorted(list(self.nodes.keys())):
            self.sorted_nodes.append(self.nodes[key])

    """
    def _refresh_nodes(self):
        #Refresh Node list
        logger.debug("Try to load nodes from another load")
        for address in self.nodes.values():
            try:
                resp = self._send_get(address, "get_nodes").json()
                self.nodes.update(resp)
            except Exception as e:
                logger.error("Request failed %s", e)
                pass

        roles = self.blockchain.get_all_roles()

        for role in roles:
            if role.role_state == "active":
                self.nodes.update({role.role_id: role.address})

        self._update_sorted_nodes()
    """

    def _get_living_node_address(self) -> str:
        """returns a living node address"""
        node = self._get_living_nodes()
        if node == {}:
            logger.error("No living node exists!")
            raise RuntimeError("No living node exists")

        return list(node.values())[0]

    def _get_living_nodes(self, count: int = 1) -> dict:
        foreign_nodes = self.nodes.copy()
        if Crypto.get_authority_id() in foreign_nodes:
            foreign_nodes.pop(Crypto.get_authority_id())

        logger.debug("Test Connection to nodes")
        for node_id in foreign_nodes.copy():  # TODO different place
            logger.debug("Try connection to node %i" % node_id)
            if self._test_connection(foreign_nodes[node_id]):
                logger.debug("Connection to node %i was successful" % node_id)
            else:
                del foreign_nodes[node_id]
                logger.debug("Connection to node %i was not successful" % node_id)

        if count > len(foreign_nodes):
            count = len(foreign_nodes)
        if len(foreign_nodes) == 0:
            logger.warning("Nodes are empty")
            return {}

        rand_nodes = {}
        for i in random.sample(list(foreign_nodes.items()), k=count):
            rand_nodes[i[0]] = i[1]

        return rand_nodes

    def _test_connection(self, address):
        try:
            req = self._send_get(address, "ping", timeout=Settings.PING_TIMEOUT)
            if req.status_code == 200:
                logger.debug("Connecting to %s was valid" % address)
                return True
        except (requests.ConnectionError, requests.ReadTimeout):
            logger.debug("Connection to %s was not valid" % address)
            return False

    def get_blockchain(self, start_at=0):  # TODO: Add threading
        logger.debug("Try to get blockchain")
        i = start_at
        tries = Settings.COUNT_TRIES_BLOCKCHAIN
        while tries > 0:
            try:
                logger.debug("Try to get block %i" % i)
                block = self.get_block(i)

                if block is None:
                    tries -= 1

                else:
                    if self.blockchain.validate_block(block):
                        logger.info("Adding block %i to the blockchain" % block.index)
                        self.blockchain.add_block(block)
                        self.load_nodes()
                        tries = Settings.COUNT_TRIES_BLOCKCHAIN
                        i += 1
                    else:
                        logger.error("Validation of block %i failed" % block.index)
                        tries -= 1
            except ValueError as e:
                logger.error("Exception %s" % e.args)
                tries -= 1
            except RuntimeError as e:
                logger.error("Exception %s" % e.args)
                tries -= 1

    def post_block(self, block: Block):
        logger.debug("Try to post block %i" % block.index)
        for address in self.nodes.values():
            Thread(target=self._send_block, args=(address, block)).start()

    def _send_block(self, node: str, block: Block):
        logger.debug("Sending block %i", block.index)
        self._send_post(node, "post_block", json.loads(block.to_json()), timeout=Settings.POST_BLOCK_TIMEOUT)

    def post_action(self, action: Action):
        for miner in self._get_next_miners(Settings.COUNT_POST_NEXT_MINERS):
            self._send_post(miner, "post_action", json.loads(action.to_json()))

    def _send_post(self, address: str, http_module: str, post_data: dict, timeout=None) -> requests.request:
        return requests.post(self._node_to_http(address) + "/" + http_module, json=post_data, timeout=timeout)

    def _send_get(self, address: str, http_module: str, timeout=None):
        return requests.get(self._node_to_http(address) + "/" + http_module, timeout=timeout)

    def get_block(self, index):
        """Returns block with index"""
        resp = self._send_post(self._get_living_node_address(), "get_block", {"block_index": index})
        if resp.status_code == 204:
            return None
        if resp.status_code == 400:
            logger.debug("Bad Block in get_block")
            raise ValueError("Bad Block in get_block")
        new_block = Block()
        new_block.from_json(resp.text)
        return new_block

    def post_sign(self, block: Block) -> Block:
        threaded = []
        signatures = []
        for address in list(self.nodes.values()):
            args = (signatures, address, block)
            t = Thread(name=str(address), target=self.thread_sign, args=args)
            t.start()
            threaded.append(t)
        for thread in threaded:
            thread.join()
        block.set_signatures(signatures)

        block.set_final_hash()
        block.sign_final()

        return block

    def thread_sign(self, signatures: list, address: str, block: Block):
        signature = Signature()

        try:
            signature.from_json(self._send_post(address, "sign_block", json.loads(block.to_json()), timeout=Settings.SIGN_BLOCK_TIMEOUT).text)
        except:
            logger.info("Couldn't connect to %s/sign_block - ReadingTimeout" % (address))
            return

        if Crypto.verify(block.hash, signature.signature, self.blockchain.get_role(signature.role_id).pubkey):
            signatures.append(signature)
