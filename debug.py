import argparse
import os
import shlex
import shutil
import subprocess
import time
from threading import Thread

import requests
from requests.auth import HTTPBasicAuth
from terminaltables import AsciiTable

settings_json = {}
processes = {}


def get_key(item) -> int:
    """Method for sorting"""
    return int(item[0])


def get_chain_port(n: int):
    """Returns the blockchain port"""
    return (n * 10) + 2


def get_api_port(n: int):
    """Returns the api port"""
    return (n * 10) + 1


def get_port(node_id: int) -> int:
    return 2000 + node_id * 10


def get_python3_cmd(node_id: int) -> str:
    """Returns a command"""
    port = get_port(node_id)
    return "python3 main.py -d -db debug/db/test_%i -ap %i -pp %i" % (node_id, port + 1, port + 2)


def get_process(process_args: str, node_id: int) -> subprocess.Popen:
    """Returns Process with"""
    cmd = get_python3_cmd(node_id) + " " + process_args
    cmd = shlex.split(cmd)
    return subprocess.Popen(cmd)


def wait_for_process(process_args: str, node_id: int):
    """Wait for a Node to complete the task ;will block code!"""
    get_process(process_args, node_id).wait()


def create_process(process_args: str, node_id: int):
    """Adds process to processes"""
    if node_id in processes:
        raise RuntimeError(("%i Node already running" % node_id))
    else:
        processes[node_id] = get_process(process_args, node_id)


def chmod_add_x(node_id: int):
    command = "chmod +x %s" % "debug/test_%i.sh" % node_id
    command = command.split(" ")
    subprocess.run(command)


def create_node(node_id: int, b_add_user=True):
    """Creates a node"""
    port = get_port(node_id)
    wait_for_process("-e localhost:2002 --noserver", node_id)
    with open(("debug/db/test_%i/pubkey.txt" % node_id), "r") as f:
        pubkey = f.read()
    create_process("-e localhost:2002", node_id)
    if b_add_user:
        requests.post("http://localhost:2001/chain/d_addrole",
                      data={"pubkey": pubkey, "rolelevel": "1", "address": "localhost:" + str(port + 2)},
                      auth=HTTPBasicAuth("admin", "admin"))
    with open(("debug/test_%i.sh" % node_id), "w") as f:
        f.write("#/bin/bash\n")
        f.write("cd '%s' \n" % os.getcwd())
        f.write("echo Start Server %i\n" % node_id)
        f.write("python3 main.py -db debug/db/test_%i -ap %i -pp %i -e localhost:2002" % (node_id, port + 1, port + 2))
    with open("debug/log", "a") as f:
        f.write("%i;%i;%i\n" % (node_id, port + 1, port + 2))
    chmod_add_x(node_id)


def start_one_server(node_id: int):
    create_process("-e localhost:2002", node_id)


parser = argparse.ArgumentParser(description='Blockchain-Debuger')

parser.add_argument("-c", "--createnetwork", type=int, help="Creates network")
parser.add_argument("-d", "--delete", action="store_true")
parser.add_argument("-s", "--startserver", type=int, help="Start a specific server")
parser.add_argument("-l", "--list", action="store_true", help="Shows all Units; better use with less")
parser.add_argument("-u", "--adduser", type=int, help="Adds User; -u pubkey")
parser.add_argument("-n", "--node", type=int,
                    help="On which node the command will be executed; if not with command will be ignored")
parser.add_argument("-r", "--addrole", nargs=3, help="Add Role, -r pubkey rolelevel address")
parser.add_argument("-a", "--startall", action="store_true", help="Start all server")
parser.add_argument("-v", "--validate", action="store_true", help="Validates chain n")

args = parser.parse_args()
port_api = None

if args.addrole and (args.node == 0 or args.node):
    values = args.adduser
    url = "https://localhost:%i/chain/addrole" % get_chain_port(args.node)
    requests.post(url, data={
        "pubkey": values[0],
        "rolelevel": values[1],
        "address": values[2]
    })
    print("Add Role")

if args.adduser and args.node:
    values = args.adduser
    url = "https://localhost:%i/chain/adduser" % get_api_port(args.node)
    requests.post(url, data={
        "pubkey": values[0]
    })
    print("Add User")

if args.delete and os.path.exists("debug"):
    shutil.rmtree(os.path.abspath("debug"))

if args.createnetwork:
    if os.path.exists("debug"):
        print("Debug Folder exist!")
        print("--createnetwork will not work!")
        exit()
    node_number = 0
    os.mkdir("debug")
    create_process("-e localhost:2002", node_number)
    time.sleep(1)
    number = args.createnetwork
    with open("debug/log", "w") as f:
        f.write("%i;%i;%i\n" % (0, 2001, 2002))
    with open(("debug/test_%i.sh" % node_number), "w") as f:
        f.write("#/bin/bash\n")
        f.write("cd '%s'\n" % os.getcwd())
        f.write("echo Start Server %i\n" % node_number)
        f.write("python3 main.py -d -db debug/db/test_%i -ap %i -pp %i -e localhost:2002" % (
            node_number, 2000 + 1, 2000 + 2))
    chmod_add_x(node_number)
    time.sleep(2)
    threads = []
    for node_number in range(1, number):
        thread = Thread(target=create_node, args=(node_number,))
        thread.start()
        threads.append(thread)

    for thread in threads:
        thread.join()

if args.list:
    table = []
    with open("debug/log", "r") as f:
        for line in f:
            line = line[:-1]
            node = line.split(";")
            node[0] = int(node[0])
            table.append(node)
    table = sorted(table, key=get_key)
    table.insert(0, ["ID", "Port API-Server", "Port Blockchain"])
    table = AsciiTable(table)
    print(table.table)

if args.startserver == 0 or args.startserver:
    start_one_server(args.startserver)

if args.startall:
    biggest_node = 0
    with open("debug/log", "r") as f:
        for line in f:
            line = line[:-1]
            node = line.split(";")
            if int(node[0]) > biggest_node:
                biggest_node = int(node[0])
    threads = []
    print(biggest_node)
    for node_number in range(0, biggest_node + 1):
        start_one_server(node_number)

if args.validate and (args.node or args.node == 0):
    cmd = get_python3_cmd(args.node) + " -v --noserver"
    print(cmd)
    os.system(cmd)

print(processes)

if processes:
    input("To Stop press Enter!")
    print("Closing!")
    for node_number in processes:
        processes[node_number].kill()
