"""
This file contains all settings. Feel free to modify them to better suite your needs.

:author: Thogs
:date: 25.3.18
"""

import os
from peewee import *

from flask import jsonify


class Settings:
    # Overwriten by args
    NOSERVER = False
    LOG_LEVEL = "INFO"

    DB_DIR = os.path.abspath("db")
    # Location of Login File
    LOGIN_FILE = os.path.join(DB_DIR, "logins.db")
    # Location of Key File
    KEY_FILE = os.path.join(DB_DIR, "key.db")
    # Location of Blockchain File
    BLOCKCHAIN_FILE = os.path.join(DB_DIR, "blockchain.db")
    # Dir that have to exist
    PUBKEY_TXT = os.path.join(DB_DIR, "pubkey.txt")
    DIR_EXIST = [DB_DIR]

    # Super administrator pubkeys
    SUPER_ADMINS = [["MIGbMBAGByqGSM49AgEGBSuBBAAjA4GGAAQAgSCOt6a753yj6HO6qI972+8lcFROcc9F5WHYt+9PFR+VNsXE5aCsEX0SM9hZJA5P1Vky+ScA0+gM98rQI42MRcIAqWRLIy7Jo/fV+YvrK/7lR1FlJe+5dDN9GwIwFVtiJ0mm8a53swTwBkKNzEcJ9qoELAihD9/4m41Ji4RJ7AJfCKc=", "Tardis:5002"]]

    # Valid roles for API Server
    VALID_API_ROLES = ["admin", "user"]

    MINIMUM_SIGNATURES_PER_BLOCK = (2 / 3)

    # Dynamic stuff
    BLOCKCHAIN_DB = SqliteDatabase(BLOCKCHAIN_FILE)

    # P2P
    COUNT_POST_NEXT_MINERS = 3

    COUNT_BLOCKCHAIN_PEERS = 5

    COUNT_TRIES_BLOCKCHAIN = 5

    TIME_PER_BLOCK = 30  # 300

    ENTRY_NODE = None   # Do not change!

    # Connection Timeouts
    PING_TIMEOUT = 5

    SIGN_BLOCK_TIMEOUT = 7

    POST_BLOCK_TIMEOUT = 8

    # Curve
    ELLIPTIC_CURVE = "secp160r1"  # "secp256r1/nistp256" # "secp160r1"

    # Port P2p
    PORT_P2P = 5002
    PORT_API = 5001

    @staticmethod
    def success_json():
        return jsonify({"success": True})

    @staticmethod
    def change_dir(db_dir: str):
        Settings.DB_DIR = os.path.abspath(db_dir)
        # Location of Login File
        Settings.LOGIN_FILE = os.path.join(Settings.DB_DIR, "logins.db")
        # Location of Blockchain File
        Settings.BLOCKCHAIN_FILE = os.path.join(Settings.DB_DIR, "blockchain.db")
        Settings.KEY_FILE = os.path.join(Settings.DB_DIR, "key.db")
        Settings.PUBKEY_TXT = os.path.join(Settings.DB_DIR, "pubkey.txt")
        Settings.DIR_EXIST = [Settings.DB_DIR]
        # Dynamic stuff
        Settings.BLOCKCHAIN_DB = SqliteDatabase(Settings.BLOCKCHAIN_FILE)
