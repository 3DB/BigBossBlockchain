from peewee import *
from passlib.hash import sha256_crypt

from settings import Settings

db = SqliteDatabase(Settings.LOGIN_FILE)


class User(Model):
    username = CharField()
    pass_hash = CharField()
    role = CharField()

    def set_password(self, password: str) -> None:
        """Hash Password and saves it"""
        self.pass_hash = sha256_crypt.encrypt(password)

    def verify_password(self, password: str) -> bool:
        """return """
        return sha256_crypt.verify(password, self.pass_hash)

    def is_admin(self):
        """return True if user is admin, else False"""
        if self.role == "admin":
            return True
        else:
            return False

    class Meta:
        database = db
