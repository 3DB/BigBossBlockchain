from flask.app import Flask
from flask import abort, request, render_template, jsonify
from flask_httpauth import HTTPBasicAuth
import os

from api.database import Database
import logging
import json
from peewee import SqliteDatabase
from settings import Settings
from api.models import User
from chain.models.contract import Contract
from chain.models.charging_station import ChargingStation
from chain.models.alias_signature import AliasSignature
from chain.blockchain import Blockchain
from chain.crypto import Crypto
from p2p.p2p import P2P

# TODO check Variables
auth = HTTPBasicAuth()
api_server = Flask('API-Server')

logger = logging.getLogger(__name__)

db = SqliteDatabase(Settings.LOGIN_FILE)
blockchain = None
p2p = None
db.create_tables([User])  # TODO besseren Ort finden?


# Debug
def add_admin_login(username, password):
    """Adds one admin user, debug only"""
    logger.info("Add Admin %s", username)
    Database.add_user_to_db(username, password, "admin")


def start_api_server(_blockchain: Blockchain, _p2p: P2P):
    """Starts api server"""
    global blockchain
    global p2p
    blockchain = _blockchain
    p2p = _p2p
    api_server.run(port=Settings.PORT_API, host='0.0.0.0')
    logging.debug("PID:", os.getpid())
    logging.debug("Werkzeug subprocess:", os.environ.get("WERKZEUG_RUN_MAIN"))
    logging.debug("Inherited FD:", os.environ.get("WERKZEUG_SERVER_FD"))
    logging.info("API-Server started")


@api_server.before_request
def before_request():
    db.close()
    db.connect()


@api_server.after_request
def after_request(response):
    db.close()
    return response


@auth.verify_password
def verify_pw(username, password):
    """Tests password"""
    return Database.verify_login(username, password)


@api_server.route('/')
@auth.login_required
def test_site():
    """Serves Test Site"""
    return '<img src="https://imgs.xkcd.com/comics/ballooning.png">, %s' % auth.username()


@api_server.route('/user/adduser', methods=['POST', 'GET'])
@auth.login_required
def add_user():
    """Add User, return true if success, return false if not. 403 if not allowed"""
    values = request.values

    # Params valid?
    if not ("username" in values and "password" in values):
        logger.debug("Sending Adduser html page")
        return render_template('template.html', title="Add User to the API", keys=["username", "password"],
                               user=auth.username())
    else:
        username = values["username"]
        password = values["password"]
        logger.debug("Try to add user %s" % username)
        role = None

        # Role valid?
        if "role" in values:
            if values["role"] == "admin":
                role = "admin"
            elif values["role"] == "user":
                role = "user"
            else:
                logger.debug("%s is a bad user role", role)
                abort(400)
        else:
            role = "user"
        logger.debug("%s is going to be an %s user" % (username, role))

        if Database.is_user_admin(auth.username()):
            logger.info("Adding &s as %s", (auth.username(), role))
            return str(Database.add_user_to_db(username, password, role))
        else:
            logger.debug("%s is not an admin", auth.username())
            abort(403)


@api_server.route("/user/changepw", methods=['POST', 'GET'])
@auth.login_required
def change_pw():
    """Changes user's password"""
    values = request.values
    # Serve normal Website if no params
    if not ("username" in values and "password" in values):
        logger.debug("Sending  html")
        return render_template("template.html", title="Change Password", keys=["username", "password"],
                               user=auth.username())

    # Change Pw
    else:
        username = values["username"]
        password = values["password"]
    # Check if logged in user is admin
    if Database.is_user_admin(auth.username()):
        logger.debug("Change password of %s", username)
        return str(Database.change_pw(username, password))
    else:
        logger.debug("%s is not a admin", username)
        abort(403)


@api_server.route("/api/change_role", methods=["POST", "GET"])
@auth.login_required
def change_role():
    """Change user's role"""
    values = request.values
    if not ("username" in values and "role" in values):
        logger.debug("Sending change_role.html")
        return render_template("template.html", title="Change Role of a user", keys=["username", "role"])
    else:
        username = values["username"]
        role = values["role"]
        if Database.is_user_admin(auth.username()):
            logger.info("Changing %s to %s", (username, role))
            return str(Database.change_role(username, role))
        else:
            logger.debug("%s is not a admin", auth.username())
            abort(403)


@api_server.route("/api/remove_user", methods=["POST", "GET"])
@auth.login_required
def remove_user():
    """remove user"""
    values = request.values
    if not ("username" in values):
        logger.debug("Sending user/remove_user to %s" % auth.username())
        return render_template("template.html", title="Remove User", keys=["username"], user=auth.username())
    else:
        username = values["username"]
        if Database.is_user_admin(auth.username()):
            logger.info("Removing %s", username)
            return str(Database.remove_user(username))
        else:
            logger.debug("%s is not a admin" % auth.username())
            abort(403)


@api_server.route("/chain/add_role", methods=["POST", "GET"])
@auth.login_required
def chain_add_role():
    """Add role to user"""
    values = get_params(request)
    if not ("pubkey" in values and "role_level" in values and "address" in values and "cs_server" in values):
        logger.debug("Sending /chain/add_role to %s" % auth.username())
        return render_template("template.html", title="Add Role", keys=["pubkey", "role_level", "address", "cs_server"],
                               user=auth.username())
    else:
        pubkey = values["pubkey"]
        role_level = values["role_level"]
        address = values["address"]
        cs_server = values["cs_server"]
        p2p.post_action(blockchain.gen_add_role(pubkey, int(role_level), address, cs_server))
        logger.info("Grant role %s to %s with address %s" % (role_level, pubkey, address))
        return Settings.success_json()


@api_server.route("/chain/verify_alias", methods=["POST", "GET"])
def chain_verify_alias():
    """Checks whether alias is valid"""
    alias_signature = AliasSignature()

    try:
        alias_signature.from_json(request.get_json())
    except:
        abort(400)

    if not blockchain.is_alias_unique(alias_signature.alias):
        abort(409)

    if not alias_signature.verify_hash():
        abort(409)

    role = blockchain.get_role(alias_signature.authority_signature.role_id)
    if not Crypto.verify(alias_signature.hash, alias_signature.authority_signature.signature, role.pubkey):
        abort(409)

    return Settings.success_json()


@api_server.route("/chain/is_alias_unique", methods=["POST", "GET"])
def chain_is_alias_unique():
    if not blockchain.is_alias_unique(request.get_json()["alias_pubkey"]):
        abort(409)

    return Settings.success_json()


@api_server.route("/chain/add_contract", methods=["POST", "GET"])
def chain_add_contract():
    values = get_params(request)

    contract = Contract()

    try:
        contract.from_json(json.dumps(values))
    except:
        abort(400)

    contract.sign_final()

    p2p.post_action(blockchain.gen_add_contract(contract))

    return Settings.success_json()


@api_server.route("/chain/get_contracts", methods=["POST", "GET"])
def chain_get_contracts():
    aliases = request.get_json()

    contracts = []
    for alias in aliases:
        contract = Contract.select().join(AliasSignature).where(Contract.user_signature_id == AliasSignature.id and AliasSignature.alias == alias)
        if "Ax2U" in alias:
            x = 1
        for c in contract:
            contracts.append(c.to_json())
            Contract.delete().where(Contract.alias == alias)
    
    return jsonify(contracts)


@api_server.route("/chain/get_charging_stations", methods=["POST", "GET"])
def chain_get_charging_stations():
    values = get_params(request)
    if not ("start_id" in values):
        abort(400)

    try:
        start_id = int(values["start_id"])
    except:
        abort(400)

    charging_stations = ChargingStation.select().where(ChargingStation.id >= start_id)

    for i in range(len(charging_stations)):
        charging_stations[i] = charging_stations[i].to_json()

    return jsonify(charging_stations)


@api_server.route("/chain/change_role_state", methods=["POST", "GET"])
@auth.login_required
def chain_change_rolestate():
    values = get_params(request)
    if not ("role_id" in values and "role_state" in values):
        abort(400)

    try:
        role_id = int(values["role_id"])
    except:
        abort(400)

    role_state = values["role_state"]

    p2p.post_action(blockchain.gen_change_rolestate(role_id, role_state))

    return Settings.success_json()


@api_server.route("/chain/add_charging_station", methods=["POST", "GET"])
@auth.login_required
def chain_add_charging_station():
    values = get_params(request)
    if not ("station_id" in values and "info_address" in values and "gps_latitude" in values
            and "gps_longitude" in values and "activity_state" in values):
        abort(400)

    try:
        station_id = int(values["station_id"])
        gps_latitude = float(values["gps_latitude"])
        gps_longitude = float(values["gps_longitude"])
    except:
        abort(400)

    info_address = values["info_address"]
    activity_state = values["activity_state"]

    p2p.post_action(blockchain.gen_add_charging_station(station_id, info_address, gps_latitude, gps_longitude, activity_state))

    return Settings.success_json()

""" DEPRECATED
@api_server.route("/chain/change_charging_station_state", methods=["POST", "GET"])
@auth.login_required
def chain_change_charging_station_state():
    values = get_params(request)
    if not ("station_id" in values and "activity_state" in values):
        abort(400)

    try:
        station_id = int(values["station_id"])
    except:
        abort(400)

    activity_state = values["activity_state"]

    p2p.post_action(blockchain.gen_change_charging_station_state(station_id, activity_state))

    return Settings.success_json()
"""

@api_server.route("/chain/sign_alias", methods=["POST", "GET"])
def chain_sign_alias():
    values = get_params(request)

    alias_signature = AliasSignature()

    try:
        alias_signature.from_json(json.dumps(values))
    except:
        abort(400)

    alias_signature.set_timestamp()
    alias_signature.set_hash()
    alias_signature.sign()

    return alias_signature.to_json()


def get_params(request: request) -> dict:
    params = {}

    for key in request.values.keys():
        params[key] = request.values[key]

    json_params = request.get_json()
    if json_params is not None:
        for key in json_params.keys():
            params[key] = json_params[key]

    return params