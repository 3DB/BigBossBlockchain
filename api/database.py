from api.models import User
from settings import Settings


class Database:
    @staticmethod
    def user_exists(username: str) -> bool:
        """Returns True if user exist, else False"""
        query = User.select().where(User.username == username)
        return len(query) > 0

    # noinspection PyTypeChecker
    @staticmethod
    def get_user(username: str) -> User:
        """Returns requested User if user exists, else None"""
        query = User.select().where(User.username == username)
        if len(query) != 0:
            return query[0]
        else:
            return None

    @staticmethod
    def add_user_to_db(username: str, password: str, role: str) -> bool:
        """Adds user to the Database, True if success"""
        if not Database.user_exists(username):
            new_user = User(username=username, role=role)
            new_user.set_password(password)
            new_user.save()
            return True
        else:
            return False

    @staticmethod
    def verify_login(username: str, password: str) -> bool:
        """Test if the login credentials are correct"""
        user = Database.get_user(username)
        if user is None:
            return False
        else:
            return user.verify_password(password)

    @staticmethod
    def is_user_admin(username: str) -> bool:
        """Test if a user is admin, False if user does not exist"""
        user = Database.get_user(username)
        if user is None:
            return False
        return user.is_admin()

    @staticmethod
    def change_pw(username: str, password: str) -> bool:
        """Changes user password"""
        user = Database.get_user(username)
        if user is None:
            return False
        else:
            user.set_password(password)
            user.save()
            return True

    @staticmethod
    def change_role(username: str, role: str) -> bool:
        """Changes user role, True if success, else False"""
        if Database.user_exists(username) and Database.is_valid_role(role):
            user = Database.get_user(username)
            if user is not None:
                user.role = role
                user.save()
                return True
        return False

    @staticmethod
    def is_valid_role(role: str) -> bool:
        """True if valid role, else False"""
        for i in Settings.VALID_API_ROLES:
            if role == i:
                return True
        return False

    @staticmethod
    def remove_user(username) -> bool:
        user = Database.get_user(username)
        if user is None:
            return False
        else:
            user.delete_instance()
            return True
